package Bitext::InputFile;
use strict;
use warnings;
use 5.010;
use Moo;
use Bitext::Line;

has path => ( is  => 'ro', isa => sub {}, required => 1);
has text_fields => ( is  => 'ro', isa => sub {}, required => 1);
has last_field => ( is  => 'ro', isa => sub {}, required => 1);
has metadata_fields => ( is  => 'ro', isa => sub {});

sub BUILD
{
    my $self = shift;

    my $is_text_field = 
	sub { my $i = shift; 
	      $i eq $_ && return 1 
		  for @{$self->text_fields}; 
	      return 0;  };

    my @metadata_fields = grep { not $is_text_field->($_) } 
    (0 .. $self->last_field);
    $self->{metadata_fields} = \@metadata_fields;

    open my $FH, '<:encoding(UTF-8)', $self->path 
	|| die "Unable to open output file ".$self->path;
    $self->{fh} = $FH;
}

sub DEMOLISH
{
    my $self = shift;
    my $FH = $self->fh;
    close $FH;
}

sub fh
{
    my $self = shift;
    return $self->{fh};
}

sub next_line # TO OVERWRITE
{
    my ($self,$line) = @_;
    $line;
}

1;
