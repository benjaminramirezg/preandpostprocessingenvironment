package Bitext::InputFile::XLSX;
use strict;
use warnings;
use 5.010;
use Moo;
use Text::Iconv;
use Bitext::Line;
use Spreadsheet::ParseXLSX;

#extends 'Bitext::InputFile';

has path => ( is  => 'ro', isa => sub {}, required => 1);
has text_fields => ( is  => 'ro', isa => sub {}, required => 1);
has last_field => ( is  => 'ro', isa => sub {}, required => 1);
has metadata_fields => ( is  => 'ro', isa => sub {});

my $LINES = [];

sub BUILD
{
    my $self = shift;

    my $is_text_field = 
	sub { my $i = shift; 
	      $i eq $_ && return 1 
		  for @{$self->text_fields}; 
	      return 0;  };

    my @metadata_fields = grep { not $is_text_field->($_) } 
    (0 .. $self->last_field);
    $self->{metadata_fields} = \@metadata_fields;

    my $parser = Spreadsheet::ParseXLSX->new;
    my $workbook = $parser->parse($self->path);

    for my $worksheet ( $workbook->worksheets() ) {

        my ( $row_min, $row_max ) = $worksheet->row_range();
        my ( $col_min, $col_max ) = $worksheet->col_range();

        for my $row ( $row_min .. $row_max ) {
	    my $line = [];
            for my $col ( $col_min .. $col_max ) {

                my $cell = $worksheet->get_cell( $row, $col );
		my $value = $cell ? $cell->value :  '';
		push @$line, $value;
            }

	    push @$LINES,$line;
        }
    }
}


#    my $converter = Text::Iconv -> new ("utf-8", "windows-1251");

sub next_line
{
    my $self = shift;
    my $line = shift @$LINES // return;

    my $obj_line = Bitext::Line->new(
	fields => $line,
	text_fields => $self->text_fields,
	metadata_fields => $self->metadata_fields);
    return $obj_line;
}

1;
