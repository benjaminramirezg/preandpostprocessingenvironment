package Bitext::InputFile::Raw;
use Moo;

extends 'Bitext::InputFile';

sub BUILD
{
    my $self = shift;
}

sub next_line
{
    my $self = shift;
    my $FH = $self->fh;
    my $line = readline($FH) // return;
    chomp $line;
    my $obj_line = Bitext::Line->new(
	fields => [$line],
	text_fields => [0],
	metadata_fields => []);
    return $obj_line;
}

1;
