package Bitext::InputFile::PPL;
use strict;
use warnings;
use 5.010;
use Moo;
use Bitext::Line;

#extends 'Bitext::InputFile';

has path => ( is  => 'ro', isa => sub {}, required => 1);
has text_fields => ( is  => 'ro', isa => sub {}, required => 1);
has last_field => ( is  => 'ro', isa => sub {}, required => 1);
has metadata_fields => ( is  => 'ro', isa => sub {});

my $LINES = [];

my $FSA = { 0 => { 0 => 0,
		   1 => 1,
		   2 => 0,
		   3 => 0 },
	    1 => { 0 => 1,
		   1 => 0,
		   2 => 2,
		   3 => 3},
	    2 => { 0 => 2,
		   1 => 0,
		   2 => 2,
		   3 => 3 },
	    3 => { 0 => 3, 
		   1 => 1,
		   2 => 0,
		   3 => 3 }};

sub BUILD
{
    my $self = shift;

    my $is_text_field = 
	sub { my $i = shift; 
	      $i eq $_ && return 1 
		  for @{$self->text_fields}; 
	      return 0;  };

    my @metadata_fields = grep { not $is_text_field->($_) } 
    (0 .. $self->last_field);
    $self->{metadata_fields} = \@metadata_fields;

    my $state = 0;
    my $metadata = [];

    open my $PPL, '<:encoding(UTF-8)', $self->path 
	|| die "Unable to open output file ".$self->path;

    while (my $line = <$PPL>)
    {
	chomp $line;
	$line =~ s///g;
	my $type = get_line_type($line);
	my $new_state = get_new_state($state,$type);

	if ($new_state eq 1)
	{
	    $metadata =  [get_id($line)];
	} elsif ($new_state eq 2)
	{
	    if ($line =~ /\S/)
	    {
		push @$metadata, get_metadata($line);
	    }
	} elsif ($new_state eq 3)
	{
	    push @$LINES,[ @$metadata, $line ] if $line =~ /\S/;

	} elsif ($new_state eq 0)
	{
	    $metadata = []; $state = 0; 
	}
	$state = $new_state;
    }
    close $PPL;
}


sub next_line
{
    my $self = shift;
    my $line = shift @$LINES // return;

    my $obj_line = Bitext::Line->new(
	fields => $line,
	text_fields => $self->text_fields,
	metadata_fields => $self->metadata_fields);
    return $obj_line;
}

sub get_line_type
{
    my $line = shift;
    return 1 if $line =~ /^#{3} ID=[0-9]+\s*$/;
    return 2 if $line =~ /^#{3} [A-Za-z]+=/;
    return 3 if $line =~ /\S+/;
    return 0;
}

sub get_new_state
{
    my ($state,$type) = @_;

    my $new_state;

    eval { $new_state = $FSA->{$state}->{$type} };

    return $new_state;
}

sub get_id
{
    my $id_line = shift;
    my ($id) = $id_line =~ /^#{3} ID=([0-9]+)\s*$/;
    return $id;
}

sub get_metadata
{
    my $m_line = shift;
    my ($m) = $m_line =~ /^#{3} [A-Za-z]+=(.*)/;

    return $m;
}

1;
