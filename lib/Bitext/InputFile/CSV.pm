package Bitext::InputFile::CSV;
use Moo;
use Text::CSV;
use IO::File;
extends 'Bitext::InputFile';

has separator => ( is  => 'ro', isa => sub {}, default => ';');

sub BUILD
{
    my $self = shift;

    my $CSV = Text::CSV->new( { binary => 1, sep_char => $self->separator, keep_meta_info => 1 } )
	or die "Text::CSV error: " . Text::CSV->error_diag;

    $self->{csv} = $CSV;
}

sub csv 
{
    my $self = shift;
    return $self->{csv};
}

sub next_line
{
    my $self = shift;
    my $CSV = $self->csv;
    my $FH = $self->fh;

    my $line = $CSV->getline($FH) // return;

    $self->add_quotes($line);

    my $obj_line = Bitext::Line->new(
	fields => $line,
	text_fields => $self->text_fields,
	metadata_fields => $self->metadata_fields);
    return $obj_line;
}

sub add_quotes
{
    my ($self,$line) = @_;
    my @line = @$line;
    my $CSV = $self->csv;
    for my $i (0 .. $#line)
    {
	next unless $CSV->is_quoted($i);
	$line->[$i] = '"'.$line->[$i].'"';
    } 
    $line;
}

1;
