package Bitext::Line;
use strict;
use warnings;
use 5.010;
use Moo;

has id => ( is  => 'ro', isa => sub {});
has fields => ( is  => 'ro', isa => sub {}, required => 1);
has text_fields => ( is  => 'ro', isa => sub {}, required => 1);
has metadata_fields => ( is  => 'ro', isa => sub {}, required => 1);

sub BUILD
{
    my $self = shift;
}


sub get_field
{
    my ($self,$index) = @_;
    $index //= 0;
    $self->fields->[$index];
}

sub is_text_field
{
    my ($self,$index) = @_;
    $_ eq $index && return 1 
	for @{$self->text_fields};
    return 0;
}

sub is_metadata_field
{
    my ($self,$index) = @_;
    $_ eq $index && return 1 
	for @{$self->metadata_fields};
    return 0;
}

sub text_fields_list
{
    my $self = shift;
    my @fields;

    for my $i (@{$self->text_fields})
    {
	push @fields, $self->fields->[$i];
    }    

    @fields;
}


sub metadata_fields_list
{
    my $self = shift;
    my @fields;

    for my $i (@{$self->metadata_fields})
    {
	push @fields, $self->fields->[$i];
    }    

    @fields;
}


sub text_fields_with_id_list
{
    my $self = shift;
    my @out;
    my @text_fields = @{$self->text_fields};

    for my $i (0 .. $#text_fields)
    {
	push @out, { field => $self->fields->[$self->text_fields->[$i]],
		     id => $self->id->[$i] };
    }    

    @out;
}


sub current_text_field
{
    my ($self,$field) = @_;
    if (defined $field) { $self->{current_text_field} = $field; }
    $self->{current_text_field};
}

1;

