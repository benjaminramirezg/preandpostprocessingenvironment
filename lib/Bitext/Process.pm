package Bitext::Process;
use strict;
use warnings;
use 5.010;
use Moo;

has input_file => ( is  => 'ro', isa => sub {}, required => 1);
has output_file => ( is  => 'ro', isa => sub {}, required => 1);
has normalizators => ( is  => 'ro', isa => sub {}, required => 1);
has greps => ( is  => 'ro', isa => sub {}, required => 1);

sub BUILD
{
    my $self = shift;
}

sub process
{
    my $self = shift;
    my $n = 0;
    while ( my $line = $self->input_file->next_line )
    {
	my $useful = 1;
	for my $grep (@{$self->greps})
	{
	    $useful = 0 unless $grep->useful($line);
	} 

	if ($useful)
	{
	    $_->normalize($line) for @{$self->normalizators};
	    $self->output_file->write_line($line);
	}
    }
}

1;
