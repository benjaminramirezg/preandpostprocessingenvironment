package Bitext::OutputFile::CSVCategories;
use strict;
use warnings;
use 5.010;
use Moo;

extends 'Bitext::OutputFile';

sub BUILD
{
    my $self = shift;
}

sub write_line
{
    my ($self,$line,$analysis) = @_;

    my @original_fields = @{$line->fields};

    splice @original_fields, $_, 1 for @{$line->text_fields};

    my $FH = $self->fh;


    for my $sentiment_analysis (@{$analysis->{categoriesanalysis}})
    {
	my $terms = join ',', @{$sentiment_analysis->{terms}};
	$terms = '"'.$terms.'"';

	my $sentence = $sentiment_analysis->{sentence}; 
	$sentence =~ s/"/""/g;
	$sentence = "\"$sentence\"";

	my @analysis_fields = (
	$sentence, 
	$sentiment_analysis->{category},$terms);

	my @fields = (@original_fields,$line->current_text_field // (),@analysis_fields);	
	say $FH join ';', @fields;
    }
}

1;

