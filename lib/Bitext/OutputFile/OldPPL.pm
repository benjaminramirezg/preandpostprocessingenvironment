package Bitext::OutputFile::OldPPL;
use strict;
use warnings;
use 5.010;
use Moo;

extends 'Bitext::OutputFile';


sub BUILD
{
    my $self = shift;

}

sub write_line
{
    my ($self,$line) = @_;
    my $FH = $self->fh;
    say $FH $_ for @{$line->fields}; 
    print $FH "\n";
}

1;

