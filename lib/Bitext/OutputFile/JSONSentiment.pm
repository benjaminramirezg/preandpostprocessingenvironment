package Bitext::OutputFile::JSONSentiment;
use strict;
use warnings;
use 5.010;
use Moo;
use Mojo::JSON qw (encode_json);

extends 'Bitext::OutputFile';

sub BUILD
{
    my $self = shift;
    my $FH = $self->fh;
    say $FH "{\"sentimentanalysis\":[";
}

sub DEMOLISH
{
    my $self = shift;
    my $FH = $self->{fh};
    say $FH "]}";
    close $FH;
}


sub write_line
{
    my ($self,$line,$analysis) = @_;

    my $FH = $self->fh;
    my @fields = @{$line->fields};
    my $id = $fields[0];
    my $sentence = $fields[1];
    my $topic = $fields[2];
    my $ntopic = $fields[3];
    my $text = $fields[4];
    my $ntext = $fields[5];
    my $score = $fields[6];

    return 1 unless $score > 0;

    $sentence =~ s/^"//;
    $sentence =~ s/"$//;

    my $sentiment_analysis;
    $sentiment_analysis->{sentence} = $sentence; 
    $sentiment_analysis->{sentiment}->{topic} = $topic;
    $sentiment_analysis->{sentiment}->{topic_norm} = $ntopic;
    $sentiment_analysis->{sentiment}->{text} = $text;
    $sentiment_analysis->{sentiment}->{text_norm} = $ntext; 
    $sentiment_analysis->{sentiment}->{score} = $score;

    say $FH encode_json($sentiment_analysis) . ",";
}

1;

