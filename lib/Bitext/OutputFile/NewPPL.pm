package Bitext::OutputFile::NewPPL;
use strict;
use warnings;
use 5.010;
use Moo;

extends 'Bitext::OutputFile';

sub BUILD
{
    my $self = shift;

    my $A2Z = []; 

    my @A2Z; push @A2Z, $_ for ('A' .. 'Z');

    for my $A (@A2Z)
    {
	for my $B (@A2Z)
	{
	    push @$A2Z, "$A$B";
	}
    }

    $self->{a2z} = $A2Z;
}

sub a2z
{
    my $self = shift;
    $self->{a2z};
}

sub write_line
{
    my ($self,$line) = @_;

    my $A2Z = $self->a2z;
    my $FH = $self->fh;

    for my $text_fields_with_id ($line->text_fields_with_id_list)
    {
	my $id = $text_fields_with_id->{id};
	my $text = $text_fields_with_id->{field};

	say $FH "### ID=$id";
	
	my $i = 0;
	for my $metadata_field ($line->metadata_fields_list)
	{
	    $metadata_field //= '';
	    say $FH "### ".$A2Z->[$i]."=$metadata_field";
	    $i++;
	}
	say $FH $text;
	print $FH "\n";
    }
}

1;

