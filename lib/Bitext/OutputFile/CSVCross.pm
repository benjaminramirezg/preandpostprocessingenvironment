package Bitext::OutputFile::CSVCross;
use strict;
use warnings;
use 5.010;
use Moo;
use Mojo::JSON qw(encode_json decode_json);

extends 'Bitext::OutputFile';

sub BUILD
{
    my $self = shift;
}

sub write_line
{
    my ($self,$line,$analysis) = @_;

    my @original_fields = @{$line->fields};

    splice @original_fields, $_, 1 for @{$line->text_fields};

    my $FH = $self->fh;

    for my $cx_analysis (@{$analysis->{cxanalysis}})
    {
	my %cx_analysis = %$cx_analysis;
	my @analysis_fields = @$cx_analysis{qw(sentence category term topic text score polarity)};

	my @fields = (@original_fields,$line->current_text_field // (),@analysis_fields, 1); # Count useful for BI tools	
	s/;//g for @fields;
	say $FH join ';', @fields;
    }
}

1;
