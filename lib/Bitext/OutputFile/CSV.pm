package Bitext::OutputFile::CSV;
use strict;
use warnings;
use 5.010;
use Moo;

extends 'Bitext::OutputFile';

has separator => ( is  => 'ro', isa => sub {}, default => ';');

sub BUILD
{
    my $self = shift;
}

sub write_line
{
    my ($self,$line) = @_;
    my $FH = $self->fh;
    my $ids = $line->id;
    my $id; eval { $id = shift @{$line->id} };
    my @fields =  @{$line->fields};	
    if (defined $id) { @fields = ( $id, @fields ) ; } 

    my $sep = $self->separator;
    say $FH join $sep, @fields;
}

1;

