package Bitext::OutputFile::CSVEntities;
use strict;
use warnings;
use 5.010;
use Moo;

extends 'Bitext::OutputFile';

sub BUILD
{
    my $self = shift;
}

sub write_line
{
    my ($self,$line,$analysis) = @_;
    my $FH = $self->fh;
    my @original_fields = @{$line->fields};

#    splice @original_fields, $_, 1 for @{$line->text_fields};

    for my $sentiment_analysis (@{$analysis->{entitiesanalysis}})
    {
	my @analysis_fields = (
	$sentiment_analysis->{type}, 
	$sentiment_analysis->{entity}, 
	$sentiment_analysis->{entity_norm});

	my @fields = (@original_fields, $line->current_text_field // (), @analysis_fields);	
	say $FH join ';', @fields;
    }
}

1;

