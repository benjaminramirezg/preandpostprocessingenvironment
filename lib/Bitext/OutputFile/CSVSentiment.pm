package Bitext::OutputFile::CSVSentiment;
use strict;
use warnings;
use 5.010;
use Moo;

extends 'Bitext::OutputFile';

sub BUILD
{
    my $self = shift;
}

sub write_line
{
    my ($self,$line,$analysis) = @_;

    my $FH = $self->fh;
    my @original_fields = @{$line->fields};

    splice @original_fields, $_, 1 for @{$line->text_fields};

    for my $sentiment_analysis (@{$analysis->{sentimentanalysis}})
    {
	my $sentence = $sentiment_analysis->{sentence}; 
	$sentence =~ s/"/""/g;
	$sentence = "\"$sentence\"";

	my @analysis_fields = (
	$sentence, 
	$sentiment_analysis->{topic}, 
	$sentiment_analysis->{topic_norm}, 
	$sentiment_analysis->{text}, 
	$sentiment_analysis->{text_norm}, 
	$sentiment_analysis->{score});


	my @fields = (@original_fields, $line->current_text_field // (), @analysis_fields);	
	say $FH join ';', @fields;
    }
}

1;

