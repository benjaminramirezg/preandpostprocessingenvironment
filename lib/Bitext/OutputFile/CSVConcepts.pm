package Bitext::OutputFile::CSVConcepts;
use strict;
use warnings;
use 5.010;
use Moo;

extends 'Bitext::OutputFile';


sub BUILD
{
    my $self = shift;
}

sub write_line
{
    my ($self,$line,$analysis) = @_;
    my @original_fields = @{$line->fields};

    my $FH = $self->fh;

#    splice @original_fields, $_, 1 for @{$line->text_fields};

    for my $sentiment_analysis (@{$analysis->{conceptsanalysis}})
    {
	my @analysis_fields = (
	$sentiment_analysis->{type}, 
	$sentiment_analysis->{concept}, 
	$sentiment_analysis->{concept_norm});

	my @fields = (@original_fields,$line->current_text_field // (),@analysis_fields);	
	say $FH join ';', @fields;
    }
}

1;

