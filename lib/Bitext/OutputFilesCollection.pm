package Bitext::OutputFilesCollection;
use strict;
use warnings;
use 5.010;
use Moo;

has files => ( is  => 'ro', isa => sub {}, required => 1);

sub BUILD
{
    my $self = shift;
}

sub write_line # To overwrite
{
    my $self = shift;
    my @args = @_;

    $_->write_line(@args) for @{$self->files};
}

1;
