package Bitext::OutputFile;
use strict;
use warnings;
use 5.010;
use Moo;

has path => ( is  => 'ro', isa => sub {}, required => 1);



sub BUILD
{
    my $self = shift;

    open my $FH, '>:encoding(UTF-8)', $self->path 
	|| die "Unable to open output file ".$self->path;
    $self->{fh} = $FH;
}

sub DEMOLISH
{
    my $self = shift;
    my $FH = $self->{fh};
    close $FH;
}

sub write_line # To overwrite
{
    my ($self,$line) = @_;
    my $FH = $self->{fh};
    say $FH  $line;
}

sub fh
{
    my $self = shift;
    return $self->{fh};
}

1;
