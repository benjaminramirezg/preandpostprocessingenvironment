package Bitext::Analyzer::Environment;
use strict;
use warnings;
use 5.010;
use Moo;
use Mojo::UserAgent;

has environment_path => ( is  => 'ro', isa => sub {});
has environment_exec => ( is  => 'ro', isa => sub {});
has input_file => ( is  => 'ro', isa => sub {}, required => 1);
has output_file => ( is  => 'ro', isa => sub {}, required => 1);

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Analyzer';

sub analyze
{
    my $self = shift;
  
    my $input = $self->input_file->path;
    my $output = $self->output_file->path;

    chdir $self->environment_path;
    my $environment_exec = $self->environment_exec;
    system("sh $environment_exec $input 2> $output");
}

1;

