package Bitext::Analyzer::API;
use strict;
use warnings;
use 5.010;
use Moo;
use Mojo::UserAgent;

has host => ( is  => 'ro', isa => sub {});
has headers => ( is  => 'ro', isa => sub {});
has endpoint => ( is  => 'ro', isa => sub {});
has input_file => ( is  => 'ro', isa => sub {}, required => 1);
has output_file => ( is  => 'ro', isa => sub {}, required => 1);
has language => ( is  => 'ro', isa => sub {});
has codingplanid => ( is  => 'ro', isa => sub {});
has language_mapper => ( is  => 'ro', isa => sub {});
has language_metadata => ( is  => 'ro', isa => sub {});
has add_text_column_as_metadata => ( is  => 'ro', isa => sub {});

sub BUILD
{
    my $self = shift;
    $self->{ua} = Mojo::UserAgent->new();

    die "No language info provided\n" 
	unless defined $self->language || 
	( $self->language_mapper && $self->language_metadata);
}

with 'Bitext::Analyzer';

sub get_language
{
    my ($self,$line) = @_;

    return $self->language if defined $self->language;

    my $mapper = $self->language_mapper // return;
    my $col_index = $self->language_metadata // return;
    my $language = $line->get_field($col_index) // return;
    return $mapper->{$language};
}

sub analyze
{
    my $self = shift;
    my $ua = $self->{ua};
    my $n = 0;

    while ( my $line = $self->input_file->next_line )
    {  
	$n++;
	my $lang = $self->get_language($line);
	next unless defined $lang;

	for my $field_index (@{$line->text_fields})
	{
	    $line->current_text_field($field_index)
		if $self->add_text_column_as_metadata;

	    my $text = $line->get_field($field_index) // next;
	    $text =~ s/^"//; $text =~ s/"$//; $text =~ s/""/"/;
	    next unless $text =~ /\S+/;

	    $text = substr($text,0,900);
	    my $params = { language => $lang, text => $text };
	    $params->{codingplanid} = $self->codingplanid if defined $self->codingplanid; 
	    my $tx = $ua->post($self->host . $self->endpoint . '/', $self->headers,json => $params);
	    my $action_id;

	    eval { $action_id = $tx->res->json->{resultid} };
	    unless (defined $action_id) { say STDERR "Bad API response\n".$tx->req->to_string . $tx->res->to_string; next; }

	    my $analysis;
	    until ($analysis)
	    {
		$tx = $ua->get($self->host . $self->endpoint . '/' . $action_id . '/', $self->headers);
		next if $tx->res->code eq 202;
		last unless $tx->res->code eq 200;
		$analysis = $tx->res->json;
	    }
	    unless ($tx->res->code eq 200) { say STDERR "Bad API response\n".$tx->req->to_string . $tx->res->to_string; next; }
	    $self->output_file->write_line($line,$analysis);
	    print STDERR "Analized line number $n     \r";
	}
    }
}

1;

