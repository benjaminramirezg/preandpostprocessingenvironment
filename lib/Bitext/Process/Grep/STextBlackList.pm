package Bitext::Process::Grep::STextBlackList;
use strict;
use warnings;
use 5.010;
use Moo;


my $BLACK_LIST =  { "be" => 1,  "more" => 1,  "most" => 1, "really" => 1, "quite" => 1,  
		    "should" => 1,  "very" => 1, "must" => 1, 
		    "not" => 1,  "no" => 1,  "less" => 1,  "too" => 1, "have" => 1, "a bit" => 1 };

has fields => ( is  => 'ro', isa => sub {}, required => 1, default => sub { [] });

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Process::Grep';

sub useful
{
    my ($self,$line) = @_;

    my $fields = $self->fields;

    for my $i (@$fields)
    {
	my $stext = $line->fields->[$i];
	return 0 if exists ($BLACK_LIST->{$stext});
	$stext = lc($stext);
	return 0 if exists ($BLACK_LIST->{$stext});
    }

    return 1;
}



1;

