package Bitext::Process::Grep::GrepV;
use strict;
use warnings;
use 5.010;
use Moo;
has fields => ( is  => 'ro', isa => sub {}, required => 1, default => sub { [] });

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Process::Grep';

sub useful
{
    my ($self,$line) = @_;
    my $fields = $self->fields;

    my $flag = 1;    

    for my $i (@$fields)
    {
	my $field = $line->fields->[$i];
	if ($field =~ /\S/)
	{ $flag = 0; last; }
    }

    return $flag;
}



1;

