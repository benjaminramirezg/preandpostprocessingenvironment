package Bitext::Process::Normalizator::HTML4QLIK;
use strict;
use warnings;
use 5.010;
use Moo;

has fields => ( is  => 'ro', isa => sub {}, required => 1, default => sub { [] });
has polarity => ( is  => 'ro', isa => sub {}, required => 1);
has topic => ( is  => 'ro', isa => sub {}, required => 1);
has stext => ( is  => 'ro', isa => sub {}, required => 1);

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Process::Normalizator';

sub normalize
{
    my ($self,$line) = @_;

    my $fields = $self->fields;
    my $polarity = $line->fields->[$self->polarity];
    my $stext = $line->fields->[$self->stext];
    my $topic = $line->fields->[$self->topic];

    my $class = $polarity eq 'POSITIVE' ? 'spositive' : 'snegative';

    for my $i (@$fields)
    {
	my $text = $line->fields->[$i];
	$text =~ s/\b($stext|$topic)\b/<span class=\'$class\'>$1<\/span>/g;
	$line->fields->[$i] = $text;
    }
}

1;
