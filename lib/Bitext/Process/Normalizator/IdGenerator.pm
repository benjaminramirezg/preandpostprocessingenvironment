package Bitext::Process::Normalizator::IdGenerator;
use strict;
use warnings;
use 5.010;
use Moo;

sub BUILD
{
    my $self = shift;
    $self->{current_id} = -1;
}

with 'Bitext::Process::Normalizator';

sub get_id
{
    my $self = shift;
    $self->{current_id}++;
    my $id = sprintf("%06d",$self->{current_id});
    return $id;
}


sub normalize
{
    my ($self,$line) = @_;

    my $ids = [];
    
    for my $field (@{$line->text_fields})
    {
	push @$ids, $self->get_id;
    }

    $line->{id} = $ids;

}


1;

