package Bitext::Process::Normalizator::HtmlCleaner;
use strict;
use warnings;
use 5.010;
use Moo;
use HTML::Entities;

my $TAGS_GRAMMAR;

sub BUILD
{
    my $self = shift;
    $self->load_grammar;
}

with 'Bitext::Process::Normalizator';

sub load_grammar
{
    my $self = shift;

    while (my $line = <DATA>)
    {
	chomp $line;
	next unless $line =~ /\S/;
	next if $line =~ /^\s*#/;
	my ($regex,$replace) = $line =~ /^(.+)#@(.*)$/;
	push @$TAGS_GRAMMAR, [$regex,$replace];
    }
}

sub clean
{
    my ($self,$field) = @_;

    for my $tag_token (@$TAGS_GRAMMAR)
    {
	my ($regex,$to_add) = @$tag_token;
	$field =~ s/$regex/qq{"$to_add"}/eeg;
    }
    $field = decode_entities($field);
    return $field;
}



sub normalize
{
    my ($self,$line) = @_;

    for my $i (@{$line->text_fields})
    {
	$line->fields->[$i] = $self->clean($line->fields->[$i]);
    }
}


1;

__DATA__

###BASICOS
<(style)\b[^>]*>.*?</\1>#@
<(script)\b[^>]*>.*?</\1>#@
<(head)\b[^>]*>.*?</\1>#@
<head>#@
</head>#@
<(header)\b[^>]*>#@
</header>#@
<(html)\b[^>]*>#@
</html>#@
<title>#@
</title>#@\n
<(body)\b[^>]*>#@
<body>#@
</body>#@
<(div)\b[^>]*>#@
</div>#@\n
<span\b[^>]*>#@
</span>#@
<footer\b[^>]*>#@
</footer>#@
<!--([^-]*-)+?->#@
###REEMPLAZO (Ejemplo para posibles usos)
###<img([^/]*/)+?>#@@@alt
###GENERICOS
<(base)\b[^>]*>.*?</\1>#@
<(link)\b[^>]*>.*?</\1>#@
<link ([^/]*/)+?>#@
<(meta)\b[^>]*>.*?</\1>#@
<meta ([^/]*/)+?>#@
<(noscript)\b[^>]*>.*?</\1>#@
</noscript>#@
<!DOCTYPE\b[^>]*>#@
###HEADING
<(h\d)\b[^>]*>#@
</h\d>#@\n
###LINK-ANCHOR
<(a)\b[^>]*>#@
</a>#@\n
<(img)\b[^>]*>#@\n
<(img)\b([^/]*/)+?>#@\n
</img>#@\n
<(nav)\b[^>]*>#@
</nav>#@\n
###FORMULARIOS
<(textarea)\b[^>]*>.*?</\1>#@
<input ([^/]*/)+?>#@
<(input)\b[^>]*>.*?</\1>#@
<(input)\b[^>]*>#@
<(fieldset)\b[^>]*>.*?</\1>#@
</fieldset>#@\n
<(legend)\b[^>]*>.*?</\1>#@
<(select)\b[^>]*>#@
</select>#@
<(button)\b[^>]*>.*?</\1>#@
<(optgroup)\b[^>]*>.*?</\1>#@
<(form)\b[^>]*>#@\n
</form>#@\n
<(label)\b[^>]*>#@
</label>#@\n
<(option)\b[^>]*>#@\n
</option>#@
###TABLAS
<(colgroup)\b[^>]*>.*?</\1>#@
<(col)\b[^>]*>#@\n
<(col)\b([^/]*/)+?>#@\n
<(table)\b[^>]*>#@
<(tr)\b[^>]*>#@
<(th)\b[^>]*>#@
<(td)\b[^>]*>#@
<(caption)\b[^>]*>#@
<(thead)\b[^>]*>#@
<(tbody)\b[^>]*>#@
<(tfoot)\b[^>]*>#@
</table>#@
</tr>#@
</th>#@
</td>#@\n
</caption>#@
</thead>#@\n
</tbody>#@\n
</tfoot>#@\n
<(center)\b[^>]*>#@
</center>#@
###ESTRUCTURALES
<(aside)\b[^>]*>#@
</aside>#@\n
<(figure)\b[^>]*>#@
</figure>#@\n
<(section)\b[^>]*>#@
</section>#@\n
###DE LINEA
<(time)\b[^>]*>#@
</time>#@\n
<(meter)\b[^>]*>#@
</meter>#@\n
<(progress)\b[^>]*>#@
</progress>#@\n
<(nobr)\b[^>]*>#@
</nobr>#@\n
<(wbr)\b[^>]*>#@
</wbr>#@\n
###INTERACTIVOS
<(details)\b[^>]*>#@
</details>#@\n
<(datagrid)\b[^>]*>#@
</datagrid>#@\n
<(command)\b[^>]*>#@
</command>#@\n
###LISTAS
<(ol)\b[^>]*>#@
<(ul)\b[^>]*>#@
<(dl)\b[^>]*>#@
</ol>#@
</ul>#@
</dl>#@
<(li)\b[^>]*>#@
<(dt)\b[^>]*>#@
<(dd)\b[^>]*>#@
</li>#@\n
</dt>#@\n
</dd>#@\n
###FRAMES
<(frameset)\b[^>]*>.*?</\1>#@
<(frame)\b[^>]*>.*?</\1>#@
<(noframes)\b[^>]*>.*?</\1>#@
<(iframe)\b[^>]*>.*?</\1>#@
###TEXT
<(p)\b[^>]*>#@
</p>#@\n
<br([^/]*/)*?>#@\n
<hr([^/]*/)*?>#@\n
<strong[^>]*>#@
</strong>#@
<u[^>]*>#@
</u>#@
<b[^>]*>#@
</b>#@
<em[^>]*>#@
</em>#@
<i[^>]*>#@
</i>#@
<(code)\b[^>]*>.*?</\1>#@
<(source)\b[^>]*>.*?</\1>#@
<(samp)\b[^>]*>.*?</\1>#@
<(map)\b[^>]*>.*?</\1>#@
<(area)\b[^>]*>.*?</\1>#@
<(param)\b[^>]*>.*?</\1>#@
<(video)\b[^>]*>.*?</\1>#@
<(applet)\b[^>]*>.*?</\1>#@
<(var)\b[^>]*>.*?</\1>#@
<(abbr)\b[^>]*>.*?</\1>#@
<(acronym)\b[^>]*>.*?</\1>#@
<(address)\b[^>]*>.*?</\1>#@
<(bdo)\b[^>]*>.*?</\1>#@
<(q)\b[^>]*>.*?</\1>#@
</{0,1}pre>#@
</{0,1}blockquote>#@
</{0,1}cite>#@
</{0,1}big>#@
</{0,1}small>#@
</{0,1}sub>#@
</{0,1}sup>#@
</{0,1}ins>#@
</{0,1}del>#@
</{0,1}kbd>#@
</{0,1}tt>#@
</{0,1}pre>#@
</{0,1}dfn>#@
<(font)\b[^>]*>#@
</font>#@
### MEDIA
<(audio)\b[^>]*>.*?</\1>#@
<(source)\b[^>]*>.*?</\1>#@
<(embed)\b[^>]*>.*?</\1>#@
<(object)\b[^>]*>.*?</\1>#@
<(param)\b[^>]*>.*?</\1>#@
<(video)\b[^>]*>.*?</\1>#@
<(applet)\b[^>]*>.*?</\1>#@
### REDES SOCIALES
<(fb:like)\b[^>]*>.*?</\1>#@
<(fb:send)\b[^>]*>.*?</\1>#@
<(fb:comments)\b[^>]*>.*?</\1>#@
<(g:plusone)\b[^>]*>.*?</\1>#@
<(v:shape)\b[^>]*>.*?</\1>#@
<(o:p)\b[^>]*>.*?</\1>#@
### OTROS
\|#@
### BASURA
(img)\b class[^>]*>#@
# CHAPUZA PARA ESCAPAR LAS COMILLAS RESULTANTES DEL CAMBIO DE ENTITIES
&quot;?#@&quot;&quot;
