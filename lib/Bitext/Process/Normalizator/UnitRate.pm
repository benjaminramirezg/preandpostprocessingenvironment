package Bitext::Process::Normalizator::UnitRate;
use strict;
use warnings;
use 5.010;
use Moo;

has fields => ( is  => 'ro', isa => sub {}, required => 1, default => sub { [] });

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Process::Normalizator';

sub normalize
{
    my ($self,$line) = @_;

    my $fields = $self->fields;

    for my $i (@$fields)
    {
	my $ur = $line->fields->[$i];

	next if $ur =~ /[0-9]/;
	next if $ur eq 'NA';
	$line->fields->[$i] = '';
    }
}

1;
