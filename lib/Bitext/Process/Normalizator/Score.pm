package Bitext::Process::Normalizator::Score;
use strict;
use warnings;
use 5.010;
use Moo;

has fields => ( is  => 'ro', isa => sub {}, required => 1, default => sub { [] });

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Process::Normalizator';

sub normalize
{
    my ($self,$line) = @_;

    my $fields = $self->fields;

    for my $i (@$fields)
    {
	my $score = $line->fields->[$i];
	$score =~ s/[.]/,/;
	$line->fields->[$i] = $score;
    }
}

1;
