package Bitext::Process::Normalizator::NewLines;
use strict;
use warnings;
use 5.010;
use Moo;

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Process::Normalizator';

sub normalize
{
    my ($self,$line) = @_;

    for my $i (@{$line->text_fields})
    {
	my $text = $line->fields->[$i];
	$text =~ s/\n//g;
	$line->fields->[$i] = $text;
    }
    return $line;
}

1;
