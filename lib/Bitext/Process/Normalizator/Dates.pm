package Bitext::Process::Normalizator::Dates;
use strict;
use warnings;
use 5.010;
use Moo;

has fields => ( is  => 'ro', isa => sub {}, required => 1, default => sub { [] });

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Process::Normalizator';

sub normalize
{
    my ($self,$line) = @_;

    my $fields = $self->fields;
    for my $i (@$fields)
    {
	my $date = $line->fields->[$i];
	my ($new_date) = $date =~ /^(.+) /;
	$line->fields->[$i] = $new_date;
    }
}

1;
