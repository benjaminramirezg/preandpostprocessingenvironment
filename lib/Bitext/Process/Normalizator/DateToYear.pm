package Bitext::Process::Normalizator::DateToYear;
use strict;
use warnings;
use 5.010;
use Moo;

has fields => ( is  => 'ro', isa => sub {}, required => 1, default => sub { [] });

sub BUILD
{
    my $self = shift;
}

with 'Bitext::Process::Normalizator';

sub normalize
{
    my ($self,$line) = @_;

    my $fields = $self->fields;
    for my $i (@$fields)
    {
	my $date = $line->fields->[$i];
	my ($year) = $date =~ /([0-9]{4})/;
	$line->fields->[$i] = $year;
    }
}

1;
