#/usr/lib/perl
use strict;
use warnings;
use 5.010;
use lib '../lib/';
use Bitext::Process;
use Bitext::Analyzer::API;
use Bitext::InputFile::CSV;
use Bitext::InputFile::XLSX;
use Bitext::InputFile::PPL;
use Bitext::OutputFile::CSV;
use Bitext::OutputFile::CSVCross;
use Bitext::Process::Normalizator::IdGenerator;
use Bitext::Process::Normalizator::Quotes;
use Bitext::Process::Normalizator::NewLines;
use Bitext::Process::Normalizator::DateToYear;
use Bitext::Process::Normalizator::UnitRate;
use Bitext::Process::Normalizator::Dates;
use Bitext::Process::Normalizator::HTML4QLIK;
use Bitext::Process::Normalizator::LowerCase;
use Bitext::Process::Grep::STextBlackList;

binmode(STDIN, ":utf8"); # Force utf-8 input
binmode(STDOUT, ":utf8"); # Force utf-8 output
binmode(STDERR, ":utf8"); # Force utf-8 output

# ANALYSIS TYPE SETUP

my $analysis = "cx";
my $output_file_class = 'Bitext::OutputFile::CSVCross';
my $endpoint = $analysis;
my $language = 'eng';
my $codingplanid = 'hotels_ENG_0';
my $input_path = '../data/Sentiment_POC_Sample_UTF8.csv';
my $output_path = '../data/output.csv';
my $middle_path = '../data/middle.csv';
my $postproc_path = '../data/postproc.csv';
my $host = 'https://svc02.api.bitext.com/';
my $headers = { 'Content-Type' => 'application/json',
		Authorization => "bearer 8b29987e45bd42c69a71810485d935f1"};

# PROPROCESSING

{
    my $text_fields = [0]; # WITHOUT ID
    my $last_field = 1; 
    my $input_file = Bitext::InputFile::CSV->new(separator => ',', text_fields => $text_fields, last_field => $last_field, path => $input_path );
    my $output_file = Bitext::OutputFile::CSV->new(separator => ',', path => $middle_path );
    my $quotes = Bitext::Process::Normalizator::Quotes->new();
    my $newlines = Bitext::Process::Normalizator::NewLines->new();
    my $process = Bitext::Process->new(input_file => $input_file, output_file => $output_file, greps => [], normalizators => [$newlines]);
    $process->process;
    $output_file->DEMOLISH; # Close the fh of this file that will be used later
}


# API ANALYSIS

{
    my $text_fields = [0];
    my $last_field = 1; 
    my $input_file = Bitext::InputFile::CSV->new(separator => ',', text_fields => $text_fields, last_field => $last_field, path => $middle_path );
    my $output_file = $output_file_class->new(path => $output_path);
    my $API = Bitext::Analyzer::API->new(host => $host, headers => $headers, endpoint => $endpoint,
					 input_file => $input_file,output_file => $output_file,
					 language => $language, codingplanid => $codingplanid );
    $API->analyze;
    $output_file->DEMOLISH; # Close the fh of this file that will be used later
}

# POSTPROCESSING

{
    my $text_fields = [8];
    my $last_field = 15; 

    my $input_file = Bitext::InputFile::CSV->new(text_fields => $text_fields, last_field => $last_field, path => $output_path );
    my $output_file = Bitext::OutputFile::CSV->new(path => $postproc_path );
    my $score = Bitext::Process::Normalizator::Score->new(fields => [13]);
    my $quotes = Bitext::Process::Normalizator::Quotes->new();
    my $dates = Bitext::Process::Normalizator::Dates->new(fields => [4,5]);
    my $unitrate = Bitext::Process::Normalizator::UnitRate->new(fields => [2]);
    my $lowercase = Bitext::Process::Normalizator::LowerCase->new(fields => [11,12]);
    my $stext = Bitext::Process::Grep::STextBlackList->new(fields => [12]);
    my $html4qlik = Bitext::Process::Normalizator::HTML4QLIK->new(fields => [8], polarity => 14, topic => 11, stext => 12);
    my $process = Bitext::Process->new(input_file => $input_file, output_file => $output_file, greps => [$stext], normalizators => [$quotes,$score,$dates,$unitrate,$html4qlik,$lowercase]);
    $process->process;
}

