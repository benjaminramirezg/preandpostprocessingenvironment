#/usr/lib/perl
use strict;
use warnings;
use 5.010;

binmode(STDOUT, ":utf8"); # Force utf-8 output
binmode(STDERR, ":utf8"); # Force utf-8 output
binmode(STDIN, ":utf8"); # Force utf-8 output

my $CONCEPTS = {};
my $count = 0;

while ( my $row = <>)
{
    $count++;
    print STDERR "$count lines readed                \r";
    chomp $row;
    $row =~ s/$//;
    my ($type,$concept) = $row =~ /([^;]+);[^;]+;([^;]+)$/;
    next unless defined $type && defined $concept;

    $CONCEPTS->{$concept}->{types}->{$type}++;
    $CONCEPTS->{$concept}->{total}++;
}

say "CONCEPT;TOTAL #;TYPE;TYPE #";

for my $concept (sort { $CONCEPTS->{$b}->{total} <=> $CONCEPTS->{$a}->{total} } keys %$CONCEPTS)
{
    print STDERR "$concept concept printed\n";
    my $total = $CONCEPTS->{$concept}->{total};
    for my $type (sort { $CONCEPTS->{$concept}->{types}->{$b} <=> $CONCEPTS->{$concept}->{types}->{$a} } keys %{$CONCEPTS->{$concept}->{types}})
    {
	my $partial = $CONCEPTS->{$concept}->{types}->{$type};
	say "$concept;$total;$type;$partial";
    } 
}


