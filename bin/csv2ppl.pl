#/usr/lib/perl
use strict;
use warnings;
use 5.010;
use lib '../lib/';
use Bitext::Process;
use Bitext::InputFile::CSV;
use Bitext::OutputFile::NewPPL;
use Bitext::Process::Normalizator::IdGenerator;
use Bitext::Process::Normalizator::Quotes;

binmode(STDIN, ":utf8"); # Force utf-8 input
binmode(STDOUT, ":utf8"); # Force utf-8 output
binmode(STDERR, ":utf8"); # Force utf-8 output

# ANALYSIS TYPE SETUP

my $input_path = '../data/input.ppl';
my $output_path = '../data/output.csv';
my $text_fields = [2];
my $last_field = 2; 

# PROPROCESSING

my $input_file = Bitext::InputFile::CSV->new(text_fields => $text_fields, last_field => $last_field, path => $input_path );
my $output_file = Bitext::OutputFile::PPL->new(path => $output_path );
my $quotes = Bitext::Process::Normalizator::Quotes->new();
my $id = Bitext::Process::Normalizator::IdGenerator->new();
my $process = Bitext::Process->new(input_file => $input_file, output_file => $middle_file, greps => [], normalizators => [$id,$quotes]);
$process->process;

