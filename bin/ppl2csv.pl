#/usr/lib/perl
use strict;
use warnings;
use 5.010;
use lib '../lib/';
use Bitext::Process;
use Bitext::InputFile::PPL;
use Bitext::OutputFile::CSV;

binmode(STDIN, ":utf8"); # Force utf-8 input
binmode(STDOUT, ":utf8"); # Force utf-8 output
binmode(STDERR, ":utf8"); # Force utf-8 output

# ANALYSIS TYPE SETUP

my $input_path = '../data/input.ppl';
my $output_path = '../data/output.csv';
my $text_fields = [2];
my $last_field = 2; 

# PROPROCESSING

my $input_file = Bitext::InputFile::PPL->new(text_fields => $text_fields, last_field => $last_field, path => $input_path );
my $output_file = Bitext::OutputFile::CSV->new(path => $output_path );
my $quotes = Bitext::Process::Normalizator::Quotes->new();
my $process = Bitext::Process->new(input_file => $input_file, output_file => $middle_file, greps => [], normalizators => [$quotes]);
$process->process;

