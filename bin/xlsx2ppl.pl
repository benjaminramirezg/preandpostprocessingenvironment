#/usr/lib/perl
use strict;
use warnings;
use 5.010;
use lib '../lib/';
use Bitext::Process;
use Bitext::InputFile::XLSX;
use Bitext::OutputFile::NewPPL;
use Bitext::Process::Grep::GrepV;
use Bitext::Process::Normalizator::IdGenerator;
use Bitext::Process::Normalizator::Quotes;

binmode(STDIN, ":utf8"); # Force utf-8 input
binmode(STDOUT, ":utf8"); # Force utf-8 output
binmode(STDERR, ":utf8"); # Force utf-8 output

# ANALYSIS TYPE SETUP

my $input_path = '../data/Auditar_Duplicados_20160222_v3.xlsx';
my $output_path = '../data/output.ppl';
my $text_fields = [3,4];
my $last_field = 6; 

# PROPROCESSING

my $input_file = Bitext::InputFile::XLSX->new(text_fields => $text_fields, last_field => $last_field, path => $input_path );
my $output_file = Bitext::OutputFile::NewPPL->new(path => $output_path );
my $quotes = Bitext::Process::Normalizator::Quotes->new(fields => [5,6]);
my $grep = Bitext::Process::Grep::GrepV->new(fields => [5]);
my $id = Bitext::Process::Normalizator::IdGenerator->new();
my $process = Bitext::Process->new(input_file => $input_file, output_file => $output_file, greps => [$grep], normalizators => [$id,$quotes]);
$process->process;

