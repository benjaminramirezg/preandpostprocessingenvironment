#/usr/lib/perl
use strict;
use warnings;
use 5.010;
use lib '../lib/';
use Bitext::Process;
use Bitext::Analyzer::API;
use Bitext::InputFile::CSV;
use Bitext::InputFile::PPL;
use Bitext::OutputFile::CSV;
use Bitext::OutputFile::CSVConcepts;
use Bitext::Process::Normalizator::NewLines;
use Bitext::Process::Normalizator::Quotes;

binmode(STDIN, ":utf8"); # Force utf-8 input
binmode(STDOUT, ":utf8"); # Force utf-8 output
binmode(STDERR, ":utf8"); # Force utf-8 output

# ANALYSIS TYPE SETUP

my $analysis = "concepts";
my $output_file_class = 'Bitext::OutputFile::CSVConcepts';
my $endpoint = $analysis;
my $language = 'eng';
my $codingschemaid = '';
my $input_path = '../data/Sentiment_POC_Sample_UTF8.csv';
my $output_path = '../data/output.csv';
my $middle_path = '../data/middle.csv';
my $text_fields = [0];
my $last_field = 1; 
my $host = 'https://svc02.api.bitext.com/';
my $headers = { 'Content-Type' => 'application/json',
		Authorization => "bearer 8b29987e45bd42c69a71810485d935f1"};

# PROPROCESSING

my $input_file = Bitext::InputFile::CSV->new(separator => ',', text_fields => $text_fields, last_field => $last_field, path => $input_path );
my $middle_file = Bitext::OutputFile::CSV->new(separator => ',', path => $middle_path );
my $new_lines = Bitext::Process::Normalizator::NewLines->new();
my $quotes = Bitext::Process::Normalizator::Quotes->new();
my $process = Bitext::Process->new(input_file => $input_file, output_file => $middle_file, greps => [], normalizators => [$new_lines,$quotes]);
$process->process;
$middle_file->DEMOLISH; # Close the fh of this file that will be used later

# API ANALYSIS

$input_file = Bitext::InputFile::CSV->new(separator => ',', text_fields => $text_fields, last_field => $last_field, path => $middle_path );
my $output_file = $output_file_class->new(path => $output_path);
my $API = Bitext::Analyzer::API->new(host => $host, headers => $headers, endpoint => $endpoint,
				     input_file => $input_file,output_file => $output_file,
				     language => $language );

$API->analyze;
